# heading 1
this is a test for gitlab
## heading 2
### heading 3
## example
*italic*

**bold**

***bold-italic***

[link](https://gitlab.com/samin_bassiri/testforgit)

---
> this is a quotation
---
### list
* L1
* L2
    * L21
    * L22
* L3

## picture
![picture test](https://about.gitlab.com/images/press/logo/png/gitlab-icon-rgb.png)

## flowchart
```mermaid
graph LR
   a --> b & c--> d
```
## sequenceDiagram
```mermaid
sequenceDiagram
    A->>B: Hello B, how are you?
    activate B
    B-->>A: Great!
    deactivate B
```
